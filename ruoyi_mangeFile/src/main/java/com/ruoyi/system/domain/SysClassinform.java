package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 课程信息对象 sys_classinform
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
public class SysClassinform extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @Excel(name = "")
    private String className;

    /**  */
    @Excel(name = "")
    private String classScore;

    /**  */
    @Excel(name = "")
    private String classTeacher;

    /**  */
    @Excel(name = "")
    private String classTerm;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setClassScore(String classScore) 
    {
        this.classScore = classScore;
    }

    public String getClassScore() 
    {
        return classScore;
    }
    public void setClassTeacher(String classTeacher) 
    {
        this.classTeacher = classTeacher;
    }

    public String getClassTeacher() 
    {
        return classTeacher;
    }
    public void setClassTerm(String classTerm) 
    {
        this.classTerm = classTerm;
    }

    public String getClassTerm() 
    {
        return classTerm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("className", getClassName())
            .append("classScore", getClassScore())
            .append("classTeacher", getClassTeacher())
            .append("classTerm", getClassTerm())
            .toString();
    }
}
