package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysClassinform;

/**
 * 课程信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
public interface SysClassinformMapper 
{
    /**
     * 查询课程信息
     * 
     * @param id 课程信息主键
     * @return 课程信息
     */
    public SysClassinform selectSysClassinformById(Long id);

    /**
     * 查询课程信息列表
     * 
     * @param sysClassinform 课程信息
     * @return 课程信息集合
     */
    public List<SysClassinform> selectSysClassinformList(SysClassinform sysClassinform);

    /**
     * 新增课程信息
     * 
     * @param sysClassinform 课程信息
     * @return 结果
     */
    public int insertSysClassinform(SysClassinform sysClassinform);

    /**
     * 修改课程信息
     * 
     * @param sysClassinform 课程信息
     * @return 结果
     */
    public int updateSysClassinform(SysClassinform sysClassinform);

    /**
     * 删除课程信息
     * 
     * @param id 课程信息主键
     * @return 结果
     */
    public int deleteSysClassinformById(Long id);

    /**
     * 批量删除课程信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysClassinformByIds(String[] ids);
}
