package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MangeFile;

/**
 * 文件列表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-13
 */
public interface MangeFileMapper 
{
    /**
     * 查询文件列表
     * 
     * @param id 文件列表主键
     * @return 文件列表
     */
    public MangeFile selectMangeFileById(Long id);

    /**
     * 查询文件列表列表
     * 
     * @param mangeFile 文件列表
     * @return 文件列表集合
     */
    public List<MangeFile> selectMangeFileList(MangeFile mangeFile);

    /**
     * 新增文件列表
     * 
     * @param mangeFile 文件列表
     * @return 结果
     */
    public int insertMangeFile(MangeFile mangeFile);

    /**
     * 修改文件列表
     * 
     * @param mangeFile 文件列表
     * @return 结果
     */
    public int updateMangeFile(MangeFile mangeFile);

    /**
     * 删除文件列表
     * 
     * @param id 文件列表主键
     * @return 结果
     */
    public int deleteMangeFileById(Long id);

    /**
     * 批量删除文件列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMangeFileByIds(String[] ids);
}
