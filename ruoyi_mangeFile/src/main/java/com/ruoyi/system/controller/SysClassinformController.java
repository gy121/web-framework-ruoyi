package com.ruoyi.system.controller;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysClassinform;
import com.ruoyi.system.service.ISysClassinformService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
@Controller
@RequestMapping("/system/classinform")
public class SysClassinformController extends BaseController
{
    private String prefix = "system/classinform";

    @Autowired
    private ISysClassinformService sysClassinformService;

    @RequiresPermissions("system:classinform:view")
    @GetMapping()
    public String classinform()
    {
        return prefix + "/classinform";
    }

    /**
     * 查询课程信息列表
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysClassinform sysClassinform)
    {
        startPage();
        List<SysClassinform> list = sysClassinformService.selectSysClassinformList(sysClassinform);
        return getDataTable(list);
    }

    /**
     * 导出课程信息列表
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:export")
    @Log(title = "课程信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysClassinform sysClassinform)
    {
        List<SysClassinform> list = sysClassinformService.selectSysClassinformList(sysClassinform);
        ExcelUtil<SysClassinform> util = new ExcelUtil<SysClassinform>(SysClassinform.class);
        return util.exportExcel(list, "课程信息数据");
    }

    /**
     * 新增课程信息
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存课程信息
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:add")
    @Log(title = "课程信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysClassinform sysClassinform)
    {
        return toAjax(sysClassinformService.insertSysClassinform(sysClassinform));
    }

    /**
     * 修改课程信息
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysClassinform sysClassinform = sysClassinformService.selectSysClassinformById(id);
        mmap.put("sysClassinform", sysClassinform);
        return prefix + "/edit";
    }

    /**
     * 修改保存课程信息
     */

    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:edit")
    @Log(title = "课程信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysClassinform sysClassinform)
    {
        return toAjax(sysClassinformService.updateSysClassinform(sysClassinform));
    }

    /**
     * 删除课程信息
     */
    @DataSource(value = DataSourceType.SLAVE)//切换数据源
    @RequiresPermissions("system:classinform:remove")
    @Log(title = "课程信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysClassinformService.deleteSysClassinformByIds(ids));
    }
}
