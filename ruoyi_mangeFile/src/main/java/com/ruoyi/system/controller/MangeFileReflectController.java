package com.ruoyi.system.controller;

import com.ruoyi.system.domain.MangeFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MangeFileReflectController {

    @Autowired
    private ReflectHandler reflectHandler;

    @GetMapping("/system/file/{action}")
    public void handleUserRequest(@PathVariable String action, @RequestParam MangeFile mangeFile) {
        reflectHandler.handleRequest(action, mangeFile);
    }
}
