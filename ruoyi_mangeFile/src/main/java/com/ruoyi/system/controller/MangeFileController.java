package com.ruoyi.system.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MangeFile;
import com.ruoyi.system.service.IMangeFileService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件列表Controller
 * 
 * @author ruoyi
 * @date 2024-04-13
 */
@Controller
@RequestMapping("/system/file")
public class MangeFileController extends BaseController
{
    private String prefix = "system/file";

    @Autowired
    private IMangeFileService mangeFileService;

    @RequiresPermissions("system:file:view")
    @GetMapping()
    public String file()
    {
        return prefix + "/file";
    }

    /**
     * 查询文件列表列表
     */
    @RequiresPermissions("system:file:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MangeFile mangeFile)
    {
        startPage();
        List<MangeFile> list = mangeFileService.selectMangeFileList(mangeFile);
        return getDataTable(list);
    }

    /**
     * 导出文件列表列表
     */
    @RequiresPermissions("system:file:export")
    @Log(title = "文件列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MangeFile mangeFile)
    {
        List<MangeFile> list = mangeFileService.selectMangeFileList(mangeFile);
        ExcelUtil<MangeFile> util = new ExcelUtil<MangeFile>(MangeFile.class);
        return util.exportExcel(list, "文件列表数据");
    }

    /**
     * 新增文件列表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存文件列表
     */
    @RequiresPermissions("system:file:add")
    @Log(title = "文件列表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MangeFile mangeFile)
    {
        return toAjax(mangeFileService.insertMangeFile(mangeFile));
    }

    /**
     * 修改文件列表
     */
    @RequiresPermissions("system:file:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MangeFile mangeFile = mangeFileService.selectMangeFileById(id);
        mmap.put("mangeFile", mangeFile);
        return prefix + "/edit";
    }

    /**
     * 修改保存文件列表
     */
    @RequiresPermissions("system:file:edit")
    @Log(title = "文件列表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MangeFile mangeFile)
    {
        return toAjax(mangeFileService.updateMangeFile(mangeFile));
    }

    /**
     * 删除文件列表
     */
    @RequiresPermissions("system:file:remove")
    @Log(title = "文件列表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mangeFileService.deleteMangeFileByIds(ids));
    }

    /**
     * 上传文件
     */
//    @RequiresPermissions("system:file:upload")
    @PostMapping( "/system/file/upload")
    @ResponseBody
    public AjaxResult uploadFile(MultipartFile file) throws Exception {
        try
        {
            System.out.println("*****************************************************************");
            System.out.println(file.getOriginalFilename());
            System.out.println(file.getInputStream());
            System.out.println(file.getSize());
            AjaxResult ajax = new AjaxResult();
            ajax.put("url","111111111");

            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }

    }


    /**
     * 文字问答
     */
//    @RequiresPermissions("system:file:export")
    @Log(title = "问答", businessType = BusinessType.EXPORT)
    @PostMapping("system/answer/getAnswer")
    @ResponseBody
    public String getAnswer(String questionStr)
    {
        try {
            String pyPath = "D:\\python_workplace\\kgt5\\kgt5-main\\answer.py";
            String stateStr = "D:/ideaProject/springwork/web-framework-ruoyi/upload/question.txt";

            // 执行Python文件，并传入参数
            ProcessBuilder processBuilder = new ProcessBuilder("D:\\software\\python\\python", pyPath,stateStr);
            processBuilder.redirectErrorStream(true);

            Process process = processBuilder.start();
            // 读取Python脚本的输出
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            // 等待Python脚本执行完成
            int exitCode = process.waitFor();
            if (exitCode != 0) {
                System.out.println("Python script exited with error code: " + exitCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "成功";
    }
}
