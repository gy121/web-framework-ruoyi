package com.ruoyi.system.controller;

import com.ruoyi.system.domain.MangeFile;
import com.ruoyi.system.service.IMangeFileService;
import com.ruoyi.system.service.impl.MangeFileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class ReflectHandler {

    @Autowired
    private IMangeFileService mangeFileService;


    public void handleRequest(String methodName, MangeFile mangeFile) {
        try {
            // 使用反射获取方法对象
            Method method = IMangeFileService.class.getMethod(methodName, String.class);
            // 动态调用方法
            method.invoke(mangeFileService, mangeFile);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
