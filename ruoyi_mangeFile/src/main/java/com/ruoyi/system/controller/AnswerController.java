package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MangeFile;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

@Controller
@RequestMapping("/system/answer")
public class AnswerController {

    private String prefix = "system/answer";

    @RequiresPermissions("system:answer:view")
    @GetMapping()
    public String file()
    {
        return prefix + "/answer";
    }

    /**
     * 文字问答
     */
//    @RequiresPermissions("system:file:export")
    @Log(title = "问答", businessType = BusinessType.EXPORT)
    @PostMapping("/getAnswer")
    @ResponseBody
    public String getAnswer(String question)
    {
        System.out.println(question);
        String result = null;
        if (!question.isEmpty())
        {
            writeToTxt(question);

        }

        try {
            String pyPath = "D:\\python_workplace\\kgt5\\kgt5-main\\answer.py";
            String stateStr = "D:/ideaProject/springwork/web-framework-ruoyi/upload/question.txt";

            // 执行Python文件，并传入参数
            ProcessBuilder processBuilder = new ProcessBuilder("D:\\software\\python\\python", pyPath,stateStr);
            processBuilder.redirectErrorStream(true);

            Process process = processBuilder.start();
            // 读取Python脚本的输出
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.forName("UTF-8")));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
             result = readFileToString("upload/question_answer.txt");
            System.out.println(result);
            // 等待Python脚本执行完成
            int exitCode = process.waitFor();
            if (exitCode != 0) {
                System.out.println("Python script exited with error code: " + exitCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("--------------------------------------------------------成功---------------------------------");
        return result;
    }

    /**
     * 将前端获取到的文本内容写入文件中
     * @param question
     */
    public void writeToTxt(String question)
    {
        try {
            // 指定文件输出路径和使用的编码
            FileOutputStream fos = new FileOutputStream("upload/question.txt");
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            Writer writer = new BufferedWriter(osw);

            // 写入中文字符
            writer.write(question);
            writer.close();

            System.out.println("中文写入成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 读取结果文件
     */
    public static String readFileToString(String filePath) {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
