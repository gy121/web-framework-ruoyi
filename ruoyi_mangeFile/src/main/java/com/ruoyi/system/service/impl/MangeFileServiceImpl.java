package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MangeFileMapper;
import com.ruoyi.system.domain.MangeFile;
import com.ruoyi.system.service.IMangeFileService;
import com.ruoyi.common.core.text.Convert;

/**
 * 文件列表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-13
 */
@Service
public class MangeFileServiceImpl implements IMangeFileService 
{
    @Autowired
    private MangeFileMapper mangeFileMapper;

    /**
     * 查询文件列表
     * 
     * @param id 文件列表主键
     * @return 文件列表
     */
    @Override
    public MangeFile selectMangeFileById(Long id)
    {
        return mangeFileMapper.selectMangeFileById(id);
    }

    /**
     * 查询文件列表列表
     * 
     * @param mangeFile 文件列表
     * @return 文件列表
     */
    @Override
    public List<MangeFile> selectMangeFileList(MangeFile mangeFile)
    {
        return mangeFileMapper.selectMangeFileList(mangeFile);
    }

    /**
     * 新增文件列表
     * 
     * @param mangeFile 文件列表
     * @return 结果
     */
    @Override
    public int insertMangeFile(MangeFile mangeFile)
    {
        return mangeFileMapper.insertMangeFile(mangeFile);
    }

    /**
     * 修改文件列表
     * 
     * @param mangeFile 文件列表
     * @return 结果
     */
    @Override
    public int updateMangeFile(MangeFile mangeFile)
    {
        return mangeFileMapper.updateMangeFile(mangeFile);
    }

    /**
     * 批量删除文件列表
     * 
     * @param ids 需要删除的文件列表主键
     * @return 结果
     */
    @Override
    public int deleteMangeFileByIds(String ids)
    {
        return mangeFileMapper.deleteMangeFileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文件列表信息
     * 
     * @param id 文件列表主键
     * @return 结果
     */
    @Override
    public int deleteMangeFileById(Long id)
    {
        return mangeFileMapper.deleteMangeFileById(id);
    }
}
