package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysClassinformMapper;
import com.ruoyi.system.domain.SysClassinform;
import com.ruoyi.system.service.ISysClassinformService;
import com.ruoyi.common.core.text.Convert;

/**
 * 课程信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
@Service
public class SysClassinformServiceImpl implements ISysClassinformService 
{
    @Autowired
    private SysClassinformMapper sysClassinformMapper;

    /**
     * 查询课程信息
     * 
     * @param id 课程信息主键
     * @return 课程信息
     */
    @Override
    public SysClassinform selectSysClassinformById(Long id)
    {
        return sysClassinformMapper.selectSysClassinformById(id);
    }

    /**
     * 查询课程信息列表
     * 
     * @param sysClassinform 课程信息
     * @return 课程信息
     */
    @Override
    public List<SysClassinform> selectSysClassinformList(SysClassinform sysClassinform)
    {
        return sysClassinformMapper.selectSysClassinformList(sysClassinform);
    }

    /**
     * 新增课程信息
     * 
     * @param sysClassinform 课程信息
     * @return 结果
     */
    @Override
    public int insertSysClassinform(SysClassinform sysClassinform)
    {
        return sysClassinformMapper.insertSysClassinform(sysClassinform);
    }

    /**
     * 修改课程信息
     * 
     * @param sysClassinform 课程信息
     * @return 结果
     */
    @Override
    public int updateSysClassinform(SysClassinform sysClassinform)
    {
        return sysClassinformMapper.updateSysClassinform(sysClassinform);
    }

    /**
     * 批量删除课程信息
     * 
     * @param ids 需要删除的课程信息主键
     * @return 结果
     */
    @Override
    public int deleteSysClassinformByIds(String ids)
    {
        return sysClassinformMapper.deleteSysClassinformByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除课程信息信息
     * 
     * @param id 课程信息主键
     * @return 结果
     */
    @Override
    public int deleteSysClassinformById(Long id)
    {
        return sysClassinformMapper.deleteSysClassinformById(id);
    }
}
